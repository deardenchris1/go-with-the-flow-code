package handlers

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
	"hartree.stfc.ac.uk/hbaas-server/context"
)

type BirthdayHandler struct {
	Context context.Context
}

func (h BirthdayHandler) registerEndpoints(g *echo.Group) {
	g.GET("", h.sayHello)
	g.GET("name/:name", h.sayHappyBirthdayToName)
}

func (h BirthdayHandler) sayHello(c echo.Context) error {
	message := fmt.Sprintf("Welcome! Try sending a request to '/name/{some-name}' to get started!")
	return c.JSON(
		http.StatusOK,
		NewAPIMessage(message),
	)
}

func (h BirthdayHandler) sayHappyBirthdayToName(c echo.Context) error {
	name := c.Param("name")
	message := fmt.Sprintf("Happy birthday %s!", name)
	return c.JSON(
		http.StatusOK,
		NewAPIMessage(message),
	)
}
