package context

import "time"

type BirthDay struct {
	Day   int
	Month time.Month
}

func NewBirthDay(date time.Time) BirthDay {
	return BirthDay{
		Day:   date.Day(),
		Month: date.Month(),
	}
}

type Context struct {
	PeopleByBirthday map[BirthDay][]string
}
